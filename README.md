# Wikipedia Category Readability
Look up the component pages of a Wikipedia category and determine readability using the [Flesch-Kincaid Reading Ease](https://en.wikipedia.org/wiki/Flesch%E2%80%93Kincaid_readability_tests) measure applied to a sample of text.

Implemented with [GuzzleHttp](https://github.com/guzzle/guzzle) and [TextStastistics](TextStastistics) packages.
