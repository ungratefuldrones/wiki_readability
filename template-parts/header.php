<?php ?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"><!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" type="text/css"><!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" type="text/css">

    <title></title>
</head>

<body>
    <div class="jumbotron">
        <div class="container">
            <h1>Wikipedia Category Readability</h1>

            <p>Look up the component pages of a Wikipedia category and determine readability using the <a href="https://en.wikipedia.org/wiki/Flesch%E2%80%93Kincaid_readability_tests">Flesch-Kincaid Reading Ease</a> measure applied to a sample of text. Implemented with <a href="https://github.com/guzzle/guzzle">GuzzleHttp</a> and <a href="https://github.com/DaveChild/Text-Statistics">TextStastistics</a> packages.</p>

            <form>
                <div class="form-group">
                    <input type="text" name="category" class="form-control" placeholder="Enter a category to query" value="<?php print isset( $_GET["category"] ) ? $_GET["category"] : "" ?>"> 
                    <label class="radio-inline">
                    	<input type="radio" name="fullText" id="inlineRadio1" value="0" <?php print (bool) $_GET["fullText"] ? "" : "checked=\"checked\"" ?> > Analyze introductory paragraph</label> 
                    <label class="radio-inline">
                    <input type="radio" name="fullText" id="inlineRadio2" value="1" <?php print (bool) $_GET["fullText"] ? "checked=\"checked\"" : "" ?> > Analyze full text (slower)</label> 
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </form>
        </div>
    </div>

    <div class="container">
