<?php
namespace wiki;

if ( isset( $_GET["category"] )) {
	// Create instance of Category with the given category name and the full text option
	$category = new classes\Category( $_GET["category"], (bool) $_GET["fullText"] );
	if ( $category->error ) {
		print "<h1>Category \"$category->title\" not found.</h1>" . PHP_EOL;
	} else {
		print "<h1>Category: $category->title</h1>" . PHP_EOL;
		print "<hr/>" . PHP_EOL;
		print "<h3>Least Readable Pages</h3>" . PHP_EOL;
		print "<ol>" . PHP_EOL;
		// Loop through member list and display scores
		foreach( $category->members as $member ) {
			print "<li>";
			print "<a href=\"$member->url\"><h4>$member->title</h4></a>" . PHP_EOL;
			print "<h5>Flesch-Kincaid Reading Ease Score: {$member->readability()} </h5>" . PHP_EOL;
			print "</li>" . PHP_EOL;
		}
		print "</ol>" . PHP_EOL;
	}
}