<?php 

/**
 * @package Wiki Category Readability
 * @author Chris Baronavski <chris@punc.tilio.us>
 * @version 0.1
 */

namespace wiki;

// Define Wikipedia API endpoint
define( "API_ENDPOINT", "https://en.wikipedia.org/w/api.php" );
// Define category member limit
define( "CM_LIMIT", 50 );
// Define extracts limit
define( "EX_LIMIT", 20 );
// Define current directory name
define( "DIRNAME", dirname(__FILE__) );

// Require vendor libraries
require_once DIRNAME . "/vendor/text-statistics/autoloader.php";
require_once DIRNAME . "/vendor/guzzle/autoloader.php";

// Require classes
require_once DIRNAME . "/classes/WikiApi.php";
require_once DIRNAME . "/classes/Category.php";
require_once DIRNAME . "/classes/CategoryMember.php";

// Require tempate header
require_once DIRNAME .  "/template-parts/header.php";
// Require analysis template
require_once DIRNAME .  "/template-parts/display-analysis.php";
// Require tempate footer
require_once DIRNAME . "/template-parts/footer.php";

?>
