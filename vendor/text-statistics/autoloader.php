<?php

$mapping = array(
    'DaveChild\TextStatistics\TextStatistics' => __DIR__ . '/src/DaveChild/TextStatistics/TextStatistics.php',
    'DaveChild\TextStatistics\Text' => __DIR__ . '/src/DaveChild/TextStatistics/Text.php',
    'DaveChild\TextStatistics\Syllables' => __DIR__ . '/src/DaveChild/TextStatistics/Syllables.php',
    'DaveChild\TextStatistics\Resource' => __DIR__ . '/src/DaveChild/TextStatistics/Resource.php',
    'DaveChild\TextStatistics\Pluralise' => __DIR__ . '/src/DaveChild/TextStatistics/Pluralise.php',
    'DaveChild\TextStatistics\Maths' => __DIR__ . '/src/DaveChild/TextStatistics/Maths.php',
);

spl_autoload_register(function ($class) use ($mapping) {
    if (isset($mapping[$class])) {
        require $mapping[$class];
    }
}, true);

