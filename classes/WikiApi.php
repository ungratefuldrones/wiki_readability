<?php 

namespace wiki\classes;
use GuzzleHttp\Client as Client;

/**
 * Minimum abstraction wrapper for Wikipedia API
 * @package Wiki Category Readability
 * @author Chris Baronavski <chris@punc.tilio.us>
 * @version 0.1
 * @todo Include error handling
 */
class WikiAPI {
    /**
     * @var array $headers Headers to include in API request
     * @var object $response Last response object
     */
	private $headers, $last_response;

    /**
     * Create a new instance
     * @param string $api_key Your MailChimp API key
     */
    public function __construct () {
    	// Set request headers
    	$this->headers = array(
	        "User-Agent" => "Wikipedia Readability/.1 <chris@punc.tilio.us>",
	        "Accept"     => "application/json"
	    );
	}

    /**
     * Make a request to the API
     * @param array $args query variables for API
     */
	public function request ( array $args ) {
		// Instantiate client
		$client  = new Client();
		// Apply default query parameters to arg list
		$args = array_merge ( $args, array( 
				"action" => "query",
				"format" => "json"
			)
		);
		// Initiate request
		$response = $client->request( "GET", API_ENDPOINT, array(  
				"headers" => $this->headers, 
				"query" => $args 
			)
		);
		// Store repsonse body
		$this->last_response = $response->getBody();
	}

    /**
     * Convert json response body to std object or array
     * @param bool $use_array default false 
     * @return  array|object		
     */
	public function response ( $use_array=false ) {
		return json_decode( $this->last_response, $use_array );
	}
}