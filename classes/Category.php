<?php

namespace wiki\classes;

/**
 * Minimum representation of Wikipedia category
 * and constituent member pages
 * @package Wiki Category Readability
 * @author Chris Baronavski <chris@punc.tilio.us>
 * @version 0.1
 */
class Category {
    /**
     * @var string $title Category title
     */
	public $title = '';
	/**
     * @var array $members List of constituent pages
     */
	public $members = array();
    /**
     * @var bool $error Does the category exist?
     */
	public $error = false;
    /**
     * @var array $memberids List of pageids of category members
     */
	private $memberids = array();

    /**
     * Create a new Category instance
     * @param string $category_name
     * @param bool $full_text Use full text extract for category members
     */
	public function __construct ( $category_name, $full_text = false ) {
		$this->title = $category_name;
		// Instantiate WikiAPI client
		$this->api = new WikiAPI;
		// Set arguments
		$args = array(
			"list" => "categorymembers",
			"cmtitle" => "Category: $this->title",
			"cmlimit" => CM_LIMIT
		);
		// Initiate request
		$this->api->request( $args );
		$response = $this->api->response();
		// If no category members, set error status to true otherwise create members
		if ( count( $response->query->categorymembers ) == 0 ){
			$this->error = true;
		} else {
			foreach ( $response->query->categorymembers as $member ) {
				if ( $member->ns == 0 ) {
					$this->memberids[] = $member->pageid;
				}
			}
			//Create members
			$this->createMembers( $full_text );
			// Sort member list by readability score
			$this->sortMembers();
		}
	}

    /**
     * Create category members
     * @param bool $full_text
     */
	private function createMembers ( $full_text = false ) {
		$members = array();
		// Set default arguments
		$member_args = array(
			"pageids" => implode( "|", $this->memberids ),
			"prop" => "extracts|info",
			"exsectionformat" => "plain",
			"explaintext" => "",
			"inprop" => "url"
		);
		// If not examining full text, include the exintro argument
		if ( !$full_text ) {
			$member_args = array_merge( $member_args, array(
				"exintro" => ""
				)
			);
		}
 		// If examining full text, set the extract limit to 1.
	    // Otherwise use the defined extract limit
		$limit = ( $full_text ) ? 1 : EX_LIMIT;

 		// Query the API until extracts for all category members are fetched
		for ( $i = 0; $i < count( $this->memberids ); $i += $limit ) {
			// Merge excontinue offset with default argument list
			$args = array_merge( $member_args, array(
				"excontinue" => $i
				)
			);
			// Initiate request
			$this->api->request( $args );
			// Merge responses
			$members = array_replace_recursive( $members,
        $this->api->response( true )["query"]["pages"] );	
		}
		// Create members
		foreach ( $members as $k => $v ) {
			$this->createMember( $k, $v["title"], $v["extract"], $v["canonicalurl"] );
		}
	}

    /**
     * Create category member
     * @param string $pageid
     * @param string $title
     * @param string $text
     * @param string $url
     */
	private function createMember ( $pageid, $title, $text, $url ) {
		$this->members[] = new CategoryMember( $pageid, $title, $text, $url );
	}
   
    /**
     * Sort members by readability score
     */
	private function sortMembers () {
		usort( $this->members, function( $a, $b ) {
		    return $a->readability() > $b->readability();
		});
	}

    /**
     * Calculate average readability of category members
     * @return  float
     */
	public function average_readability () {
		$sum = array_reduce( $this->members, function( $i, $obj ) {
			return $i += $obj->readability();
		});
		return $sum / count( $this->members );
	}
}
