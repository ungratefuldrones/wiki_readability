<?php

namespace wiki\classes;
use DaveChild\TextStatistics as TS;

/**
 * Category Member 
 * @package Wiki Category Readability
 * @author Chris Baronavski <chris@punc.tilio.us>
 * @version 0.1
 * @todo Choose among multiple readability algorithms
 */
class CategoryMember {
    /**
     * @var string $title Title of the page
     */	
	public $title = '';
    /**
     * @var string $text Full text extract
     */
	public $text = '';
    /**
     * @var string $url Canonical url for page
     */
	public $url = '';
    /**
     * @var string $pageid Pageid for category member
     * @var float $readability Readability score
     */
	private $pageid, $readability;

    /**
     * Create a new CategoryMember instance
     * @param string $pageid
     * @param string $title
	 * @param string $intro          
	 * @param string $url
     * @param string $text
     */
	public function __construct ( $pageid, $title, $text, $url ) {
		$this->pageid  = $pageid;
		$this->title = $title;
		$this->text = $text;
		$this->url = $url;
	}

    /**
     * Calculate readability
     * @return float 
 	 */
	public function readability () {
		
		//  Only conduct expensive calculations once
		if ( empty( $this->readability )) {
			$textStatistics = new TS\TextStatistics;
			$this->readability = $textStatistics->fleschKincaidReadingEase( $this->text );
		}

		return $this->readability;
	}
}
